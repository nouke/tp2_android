package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {

    private BookRepository repository;
    private MutableLiveData<Book> searchResults;


    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        searchResults = repository.getSelectedBook();
    }

    public MutableLiveData<Book> getSelectedBook() {
        return searchResults;
    }

    public void findBook(long id) {
        repository.getBook(id);
    }

    public void insertOrUpdateBook(Book book){
        if(book.getId() != 0) {
            repository.updateBook(book);
        }else{
            repository.insertBook(book);
        }
    }
}
